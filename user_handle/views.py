from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from lukisan_handle.models import Lukisan
from user_handle.models import Staff, Pelanggan, Bank
from user_handle.decorator import login_required
import json
import datetime


def landing(request):
    lukisan_list = Lukisan().getAll()
    paginator = Paginator(lukisan_list, 5)
    page = request.GET.get('page')
    lukisan = paginator.get_page(page)
    return render(request, "landing/landing.html", {
        "lukisan": lukisan
    })


def login_staff(request):

    if request.method == "POST":

        staff_id = request.POST["staff_id"]
        password = request.POST["password"]
        staff = Staff().authenticate(staff_id, password)
        if staff:

            request.session["user"] = json.dumps(staff)
            request.session["staff"] = staff["no_staff"]
            request.session.modified = True

    return redirect("user:landing")


@login_required
def list_pengguna(request):

    pemilik = Staff().get_pemilik()
    penyewa = Staff().get_penyewa()
    bank = Bank().getAll()

    return render(request, "user_handle/list_pengguna.html", {
        "list_pemilik": pemilik,
        "list_penyewa": penyewa,
        "list_bank": bank
    })


@login_required
def list_staff(request):

    staff = Staff().getAll()

    return render(request, "user_handle/list_staff.html", {
        "list_staff": staff
    })


@login_required
def logout_staff(request):
    try:
        del request.session['staff']
        del request.session['user']
    except KeyError:
        pass
    return redirect("user:landing")


def default(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()


def login_pelanggan(request):

    if request.method == "POST":

        user_id = request.POST["user_id"]
        password = request.POST["password"]
        user_obj = Pelanggan()
        user = user_obj.authenticate(user_id, password)
        if user:
            user["pemilik"] = user_obj.check_role(user_id)[0]["pemilik"]
            request.session["user"] = json.dumps(user,
                                                 sort_keys=True,
                                                 indent=1,
                                                 default=default)
            request.session["pemilik"] = user["pemilik"]
            request.session.modified = True

    return redirect("user:landing")


@login_required
def profile_staff(request, id):

    user = Staff().get_profile(id)

    print(user)

    return render(request, "user_handle/profile.html", {
        "user": user
    })


def daftar_pelanggan_pemilik(request):

    if request.method == "POST":

        user_obj = Pelanggan()
        print(request.POST)
        name = request.POST["nama"]
        email = request.POST["email"]
        password = request.POST["password"]
        no_ktp = request.POST["no_ktp"]
        raw_ttl = request.POST["ttl"]
        no_hp = request.POST["no_hp"]
        alamat = request.POST["alamat"]
        no_rek = request.POST["no_rek"]
        nama_bank = request.POST["bank"]

        user = user_obj.create_pelanggan_pemilik(
            name, password, no_ktp, alamat, raw_ttl, email, no_hp, no_rek, nama_bank)

    return redirect("user:list_pengguna")


def daftar_pelanggan_penyewa(request):

    if request.method == "POST":

        user_obj = Pelanggan()

        name = request.POST["nama"]
        email = request.POST["email"]
        password = request.POST["password"]
        no_ktp = request.POST["no_ktp"]
        raw_ttl = request.POST["ttl"]
        no_hp = request.POST["no_hp"]
        alamat = request.POST["alamat"]
        kategori = request.POST["kategori"]
        pekerjaan = request.POST["pekerjaan"]

        user = user_obj.create_pelanggan_penyewa(
            name, password, no_ktp, alamat, raw_ttl, email, no_hp, kategori, pekerjaan)

    return redirect("user:list_pengguna")


@login_required
def logout_pelanggan(request):
    try:
        # del request.session['staff']
        del request.session['user']
    except KeyError:
        pass
    return redirect("user:landing")


@login_required
def profile(request, id):

    user = Pelanggan().get_profile(id)

    print(user)

    return render(request, "user_handle/profile.html", {
        "user": user
    })


def list_mitra_bank(request):

    bank = Bank().getAll()

    return render(request, "user_handle/list_bank.html", {
        "list_bank": bank
    })


def add_mitra_bank(request):

    if request.method == "POST":

        bank_obj = Bank()

        nama_bank = request.POST["nama"]
        bank_obj.create_mitra(nama_bank)

    return redirect("user:list_bank")


def delete_bank(request, id):

    print("hai")
    Bank().delete_mitra(id)

    return redirect("user:list_bank")
