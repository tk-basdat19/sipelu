from django.db import connection
from django.contrib.auth.hashers import make_password, check_password


class Staff():

    cursor = None

    def __init__(self, *args, **kwargs):
        self.cursor = connection.cursor()
        self.cursor.execute("SET SEARCH_PATH TO sipelu")

    def getAll(self):
        self.cursor.execute(
            "SELECT s.*, sl.* FROM staff s join staff_lulusan sl on s.no_staff = sl.no_staff")
        res = self.serialize(self.cursor)
        return res

    def get_profile(self, id):
        self.cursor.execute(
            "SELECT * FROM staff WHERE no_staff = %s", [id])
        res = self.serialize(self.cursor)
        return res[0]

    def authenticate(self, id, password):
        self.cursor.execute(
            "SELECT no_staff,nama,no_hp,email,password FROM staff WHERE no_staff= %s", [id])
        res = self.serialize(self.cursor)

        if (len(res) > 0 and check_password(password, res[0]["password"])):
            return res[0]
        return None

    def get_penyewa(self):
        self.cursor.execute(
            "SELECT pl.*, pn.* FROM pelanggan pl join penyewa pn on pl.id_pelanggan = pn.id_pelanggan")
        res = self.serialize(self.cursor)
        return res

    def get_pemilik(self):
        self.cursor.execute(
            "SELECT pl.*, pk.* FROM pelanggan pl join pemilik pk on pl.id_pelanggan = pk.id_pelanggan")
        res = self.serialize(self.cursor)
        return res

    def get_sewa(self):
        self.cursor.execute(
            "select a.*, b.* from (select ts.*, bs.* from transaksi_sewa ts join biaya_sewa bs on ts.tgl_sewa = bs.tgl_sewa) a join (select tb.*, mb.* from tenor_bayar tb join mitra_bank mb on tb.kode_bank = mb.kode_bank ) b on a.kode_tenor = b.kode_tenor"
        )
        res = self.serialize(self.cursor)
        return res

    def get_transaksi(self):
        self.cursor.execute(
            "select f.tgl_terima, f.jumlah, f.id_pelanggan, l.harga, (l.harga * f.jumlah) as total from fee f join lukisan l on f.id_pelanggan = l.id_pelanggan;")
        res = self.serialize(self.cursor)
        return res

    def create_super_user(self):
        self.cursor.execute("UPDATE staff SET nama=%s, password= %s WHERE no_staff = %s", [
            "Anggardha Febriano", make_password("test12345"), "STF0000001"])

    def serialize(self, cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]


class Pelanggan():

    cursor = None

    def __init__(self, *args, **kwargs):
        self.cursor = connection.cursor()
        self.cursor.execute("SET SEARCH_PATH TO sipelu")

    def getAll(self):
        self.cursor.execute("SELECT * FROM pelanggan")
        res = self.serialize(self.cursor)
        return res

    def get_profile(self, id):
        self.cursor.execute(
            "SELECT * FROM pelanggan WHERE id_pelanggan = %s", [id])
        res = self.serialize(self.cursor)
        return res[0]

    def generate_id(self):
        all_user = len(self.getAll())
        new_user = str(all_user + 1)
        zero = ""
        for i in range(len(new_user), 7):
            zero += "0"

        return "USR" + zero + new_user

    def create_pelanggan_pemilik(self, nama, password, no_ktp, alamat, ttl, email, no_hp, no_rek, nama_bank):
        id = self.generate_id()
        self.cursor.execute("INSERT INTO pelanggan VALUES (%s,%s,%s,%s,%s,%s,%s,%s)", [
            id, no_ktp, nama, alamat, ttl, no_hp, email, make_password(password)])

        self.cursor.execute("INSERT INTO pemilik VALUES (%s, %s, %s)", [
                            id, no_rek, nama_bank])

    def create_pelanggan_penyewa(self, nama, password, no_ktp, alamat, ttl, email, no_hp, pekerjaan, kategori):
        id = self.generate_id()
        self.cursor.execute("INSERT INTO pelanggan VALUES (%s,%s,%s,%s,%s,%s,%s,%s)", [
            id, no_ktp, nama, alamat, ttl, no_hp, email, make_password(password)])

        self.cursor.execute("INSERT INTO penyewa VALUES (%s, %s, %s)", [
                            id, pekerjaan, kategori])

    def create_super_user(self):
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO sipelu")
            cursor.execute("UPDATE pelanggan SET nama=%s, password= %s WHERE id_pelanggan = %s", [
                           "Anggardha Febriano", make_password("test12345"), "USR0000001"])

    def authenticate(self, id, password):
        self.cursor.execute(
            "SELECT * FROM pelanggan WHERE id_pelanggan= %s", [id])
        res = self.serialize(self.cursor)

        if (len(res) > 0 and check_password(password, res[0]["password"])):
            return res[0]
        return None

    def check_role(self, id):
        self.cursor.execute(
            "SELECT id_pelanggan, CASE WHEN id_pelanggan IN (SELECT id_pelanggan FROM pemilik WHERE id_pelanggan = %s) THEN True else False end as pemilik from pelanggan where id_pelanggan = %s", [
                id, id]
        )
        res = self.serialize(self.cursor)

        return res

    def serialize(self, cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]


class Bank():

    cursor = None

    def __init__(self, *args, **kwargs):
        self.cursor = connection.cursor()
        self.cursor.execute("SET SEARCH_PATH TO sipelu")

    def serialize(self, cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    def getAll(self):
        self.cursor.execute(
            "SELECT * FROM mitra_bank"
        )
        res = self.serialize(self.cursor)

        return res

    def create_mitra(self, nama_bank):
        new_id = self.generate_id()
        self.cursor.execute(
            "INSERT INTO mitra_bank values (%s, %s)", [new_id, nama_bank]
        )

    def delete_mitra(self, id):
        self.cursor.execute(
            "DELETE FROM mitra_bank WHERE kode_bank = %s", [id]
        )

    def generate_id(self):
        all_mitra = len(self.getAll())
        new_mitra = str(all_mitra + 1)
        zero = ""
        for i in range(len(new_mitra), 3):
            zero += "0"

        return "B" + zero + new_mitra
