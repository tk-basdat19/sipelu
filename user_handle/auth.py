from django.shortcuts import redirect
import json


def is_auth(request):
    try:
        user = request.session['user']
    except Exception as e:
        user = None

    if (user):
        request.session['user'] = user
        return {'is_auth': json.loads(user)}
    else:
        return {'is_auth': None}


def is_staff(request):

    try:

        is_staff = False
        if request.session['staff']:
            is_staff = True

    except Exception as e:
        is_staff = False

    return {'is_staff': is_staff}


def is_pemilik(request):

    try:
        is_pemilik = False
        if request.session["pemilik"]:
            is_pemilik = True
    except Exception as e:
        is_pemilik = False

    return {'is_pemilik': is_pemilik}
