from django.urls import path, include
from .views import *

app_name = "user"

urlpatterns = [
    path('', landing, name="landing"),

    path('login_staff', login_staff, name="login_staff"),
    path('logout_staff', logout_staff, name="logout_staff"),
    path('list_pengguna', list_pengguna, name="list_pengguna"),
    path('list_staff', list_staff, name="list_staff"),
    path('list_bank', list_mitra_bank, name="list_bank"),
    path('add_bank', add_mitra_bank, name="add_bank"),
    path('delete_bank/<id>', delete_bank, name="delete_bank"),

    path('login', login_pelanggan, name="login"),
    path('logout', logout_pelanggan, name="logout"),
    path('signup_pemilik', daftar_pelanggan_pemilik, name="signup_pemilik"),
    path('signup_penyewa', daftar_pelanggan_penyewa, name="signup_penyewa"),
    path('profile_staff/<id>', profile_staff, name="profile_staff"),
    path('profile/<id>', profile, name="profile")
]
