PGDMP     )    "        	        w            lukisan #   10.6 (Ubuntu 10.6-0ubuntu0.18.10.1) #   10.6 (Ubuntu 10.6-0ubuntu0.18.10.1) �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    27946    lukisan    DATABASE     y   CREATE DATABASE lukisan WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE lukisan;
             anggardhanoano    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        2615    27996    sipelu    SCHEMA        CREATE SCHEMA sipelu;
    DROP SCHEMA sipelu;
             anggardhanoano    false                        3079    13041    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1                       1255    28496    hitung_denda()    FUNCTION     �  CREATE FUNCTION sipelu.hitung_denda() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
NEW.biaya_sewa_lukisan = floor(new.biaya_sewa_lukisan - (new.biaya_sewa_lukisan * potongan));

IF (NEW.tgl_bayar_sewa > tgl_jatuh_tempo) THEN
NEW.denda = (NEW.tgl_bayar_sewa - tgl_jatuh_tempo)%30;
else
 new.denda = 0;
end if;
old.denda = 250000 * new.denda;
new.total_biaya = new.biaya_sewa_lukisan + new.ongkir + new.denda;
RETURN NEW;
END;
$$;
 %   DROP FUNCTION sipelu.hitung_denda();
       sipelu       anggardhanoano    false    1    4            �            1255    27997    hitung_diskon()    FUNCTION     $  CREATE FUNCTION sipelu.hitung_diskon() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    kategori_pelanggan VARCHAR;
    potongan REAL;
BEGIN
    SELECT p.kategori INTO kategori_pelanggan FROM penyewa p WHERE p.id_pelanggan = NEW.id_pelanggan;

    IF (kategori_pelanggan = 'S') THEN
        potongan = 0.5;
    ELSIF (kategori_pelanggan = 'G') THEN
        potongan = 0.10;
    ELSIF (kategori_pelanggan = 'P') THEN
        potongan = 0.15; 
    ELSE
        potongan = 00.0;
    END IF;

    NEW.biaya_sewa_lukisan = FLOOR(NEW.biaya_sewa_lukisan - (NEW.biaya_sewa_lukisan * potongan));

    IF (NEW.tgl_bayar_sewa > NEW.tgl_jatuh_tempo) THEN
        NEW.denda = NEW.denda + 250000;
    END IF;

    NEW.total_biaya = NEW.biaya_sewa_lukisan + NEW.ongkir + NEW.denda;

    RETURN NEW;

END;
$$;
 &   DROP FUNCTION sipelu.hitung_diskon();
       sipelu       anggardhanoano    false    1    4                       1255    27998    hitung_fee()    FUNCTION     y  CREATE FUNCTION sipelu.hitung_fee() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE 
    jumlah_fee INTEGER;
    jumlah_bayar INTEGER;
    pilihan_tenor TEXT;
    idLukisan TEXT;
    idPemilik TEXT;
BEGIN
    SELECT COUNT(b.kode_tenor) INTO jumlah_bayar
    FROM BIAYA_SEWA b
    WHERE b.kode_tenor = NEW.kode_tenor;

    SELECT t.tenor_pilihan INTO pilihan_tenor
    FROM TENOR_BAYAR t
    WHERE t.kode_tenor = NEW.kode_tenor;

    SELECT b.total_biaya INTO jumlah_bayar
    FROM BIAYA_SEWA b
    WHERE b.tgl_sewa = NEW.tgl_sewa;

    SELECT t.id_lukisan INTO idLukisan
    FROM TRANSAKSI_SEWA t
    WHERE t.id_pelanggan = NEW.id_pelanggan;

    SELECT l.id_pelanggan INTO idPemilik
    FROM LUKISAN l
    WHERE l.id_lukisan = idLukisan;

    IF (jumlah_bayar = 12 AND substr(pilihan_tenor, 1,1) = '1') THEN
        INSERT INTO FEE VALUES(
            substr(NOW()::VARCHAR(255), 1 , 10),
            jumlah_bayar * 0.20,
            idPemilik 
        );

    ELSIF (jumlah_bayar = 4 AND substr(pilihan_tenor, 1,1) = '3') THEN
        INSERT INTO FEE VALUES(
            substr(NOW()::VARCHAR(255), 1 , 10),
            jumlah_bayar * 0.20,
            idPemilik
        );
    ELSIF (jumlah_bayar = 2 AND substr(pilihan_tenor, 1,1) = '6') THEN
        INSERT INTO FEE VALUES(
            substr(NOW()::VARCHAR(255), 1 , 10),
            jumlah_bayar * 0.20,
            idPemilik
        );
    ELSIF (jumlah_bayar = 1 AND substr(pilihan_tenor, 1,1) = '12') THEN
        INSERT INTO FEE VALUES(
            substr(NOW()::VARCHAR(255), 1 , 10),
            jumlah_bayar * 0.20,
            idPemilik
        );
    END IF;
    RETURN NEW;
END;
$$;
 #   DROP FUNCTION sipelu.hitung_fee();
       sipelu       anggardhanoano    false    4    1                       1255    27999    validasi_durasi()    FUNCTION     U  CREATE FUNCTION sipelu.validasi_durasi() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (NEW.lama_sewa < 1) THEN
        RAISE EXCEPTION '% Lama sewa minimal 1 bulan', NEW.lama_sewa;
    ELSIF (NEW.lama_sewa > 12) THEN
        RAISE EXCEPTION '% Lama sewa maksimal 12 bulan', NEW.lama_sewa;
    END IF;
    RETURN NEW;
END;
$$;
 (   DROP FUNCTION sipelu.validasi_durasi();
       sipelu       anggardhanoano    false    1    4                       1255    28000    validasi_tenor()    FUNCTION     �  CREATE FUNCTION sipelu.validasi_tenor() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (NEW.tenor_pilihan != '1 Bulan') THEN
        RAISE EXCEPTION '% Tenor tidak valid', NEW.tenor_pilihan;
    ELSIF (NEW.tenor_pilihan != '3 Bulan') THEN
        RAISE EXCEPTION '% Tenor tidak valid', NEW.tenor_pilihan;
    ELSIF (NEW.tenor_pilihan != '6 Bulan' OR NEW.tenor_pilihan != '12 Bulan') THEN
        RAISE EXCEPTION '% Tenor tidak valid', NEW.tenor_pilihan;
    END IF;
    RETURN NEW;
END;
$$;
 '   DROP FUNCTION sipelu.validasi_tenor();
       sipelu       anggardhanoano    false    4    1            �            1259    28158 
   auth_group    TABLE     f   CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);
    DROP TABLE public.auth_group;
       public         anggardhanoano    false    3            �            1259    28156    auth_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.auth_group_id_seq;
       public       anggardhanoano    false    3    215            �           0    0    auth_group_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;
            public       anggardhanoano    false    214            �            1259    28168    auth_group_permissions    TABLE     �   CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);
 *   DROP TABLE public.auth_group_permissions;
       public         anggardhanoano    false    3            �            1259    28166    auth_group_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.auth_group_permissions_id_seq;
       public       anggardhanoano    false    217    3            �           0    0    auth_group_permissions_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;
            public       anggardhanoano    false    216            �            1259    28150    auth_permission    TABLE     �   CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);
 #   DROP TABLE public.auth_permission;
       public         anggardhanoano    false    3            �            1259    28148    auth_permission_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.auth_permission_id_seq;
       public       anggardhanoano    false    3    213            �           0    0    auth_permission_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;
            public       anggardhanoano    false    212            �            1259    28176 	   auth_user    TABLE     �  CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);
    DROP TABLE public.auth_user;
       public         anggardhanoano    false    3            �            1259    28186    auth_user_groups    TABLE        CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 $   DROP TABLE public.auth_user_groups;
       public         anggardhanoano    false    3            �            1259    28184    auth_user_groups_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.auth_user_groups_id_seq;
       public       anggardhanoano    false    221    3            �           0    0    auth_user_groups_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;
            public       anggardhanoano    false    220            �            1259    28174    auth_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.auth_user_id_seq;
       public       anggardhanoano    false    3    219            �           0    0    auth_user_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;
            public       anggardhanoano    false    218            �            1259    28194    auth_user_user_permissions    TABLE     �   CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 .   DROP TABLE public.auth_user_user_permissions;
       public         anggardhanoano    false    3            �            1259    28192 !   auth_user_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.auth_user_user_permissions_id_seq;
       public       anggardhanoano    false    3    223            �           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;
            public       anggardhanoano    false    222            �            1259    28254    django_admin_log    TABLE     �  CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);
 $   DROP TABLE public.django_admin_log;
       public         anggardhanoano    false    3            �            1259    28252    django_admin_log_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.django_admin_log_id_seq;
       public       anggardhanoano    false    225    3            �           0    0    django_admin_log_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;
            public       anggardhanoano    false    224            �            1259    28140    django_content_type    TABLE     �   CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);
 '   DROP TABLE public.django_content_type;
       public         anggardhanoano    false    3            �            1259    28138    django_content_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.django_content_type_id_seq;
       public       anggardhanoano    false    3    211            �           0    0    django_content_type_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;
            public       anggardhanoano    false    210            �            1259    28129    django_migrations    TABLE     �   CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
 %   DROP TABLE public.django_migrations;
       public         anggardhanoano    false    3            �            1259    28127    django_migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.django_migrations_id_seq;
       public       anggardhanoano    false    3    209            �           0    0    django_migrations_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;
            public       anggardhanoano    false    208            �            1259    28285    django_session    TABLE     �   CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);
 "   DROP TABLE public.django_session;
       public         anggardhanoano    false    3            �            1259    28335 
   auth_group    TABLE     f   CREATE TABLE sipelu.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);
    DROP TABLE sipelu.auth_group;
       sipelu         anggardhanoano    false    4            �            1259    28333    auth_group_id_seq    SEQUENCE     �   CREATE SEQUENCE sipelu.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE sipelu.auth_group_id_seq;
       sipelu       anggardhanoano    false    4    234            �           0    0    auth_group_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE sipelu.auth_group_id_seq OWNED BY sipelu.auth_group.id;
            sipelu       anggardhanoano    false    233            �            1259    28345    auth_group_permissions    TABLE     �   CREATE TABLE sipelu.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);
 *   DROP TABLE sipelu.auth_group_permissions;
       sipelu         anggardhanoano    false    4            �            1259    28343    auth_group_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE sipelu.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE sipelu.auth_group_permissions_id_seq;
       sipelu       anggardhanoano    false    4    236            �           0    0    auth_group_permissions_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE sipelu.auth_group_permissions_id_seq OWNED BY sipelu.auth_group_permissions.id;
            sipelu       anggardhanoano    false    235            �            1259    28327    auth_permission    TABLE     �   CREATE TABLE sipelu.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);
 #   DROP TABLE sipelu.auth_permission;
       sipelu         anggardhanoano    false    4            �            1259    28325    auth_permission_id_seq    SEQUENCE     �   CREATE SEQUENCE sipelu.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE sipelu.auth_permission_id_seq;
       sipelu       anggardhanoano    false    4    232            �           0    0    auth_permission_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE sipelu.auth_permission_id_seq OWNED BY sipelu.auth_permission.id;
            sipelu       anggardhanoano    false    231            �            1259    28353 	   auth_user    TABLE     �  CREATE TABLE sipelu.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);
    DROP TABLE sipelu.auth_user;
       sipelu         anggardhanoano    false    4            �            1259    28363    auth_user_groups    TABLE        CREATE TABLE sipelu.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 $   DROP TABLE sipelu.auth_user_groups;
       sipelu         anggardhanoano    false    4            �            1259    28361    auth_user_groups_id_seq    SEQUENCE     �   CREATE SEQUENCE sipelu.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE sipelu.auth_user_groups_id_seq;
       sipelu       anggardhanoano    false    240    4            �           0    0    auth_user_groups_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE sipelu.auth_user_groups_id_seq OWNED BY sipelu.auth_user_groups.id;
            sipelu       anggardhanoano    false    239            �            1259    28351    auth_user_id_seq    SEQUENCE     �   CREATE SEQUENCE sipelu.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE sipelu.auth_user_id_seq;
       sipelu       anggardhanoano    false    4    238            �           0    0    auth_user_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE sipelu.auth_user_id_seq OWNED BY sipelu.auth_user.id;
            sipelu       anggardhanoano    false    237            �            1259    28371    auth_user_user_permissions    TABLE     �   CREATE TABLE sipelu.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 .   DROP TABLE sipelu.auth_user_user_permissions;
       sipelu         anggardhanoano    false    4            �            1259    28369 !   auth_user_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE sipelu.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE sipelu.auth_user_user_permissions_id_seq;
       sipelu       anggardhanoano    false    4    242            �           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE sipelu.auth_user_user_permissions_id_seq OWNED BY sipelu.auth_user_user_permissions.id;
            sipelu       anggardhanoano    false    241            �            1259    28001 
   biaya_sewa    TABLE     �  CREATE TABLE sipelu.biaya_sewa (
    tgl_sewa date NOT NULL,
    biaya_sewa_lukisan integer NOT NULL,
    alamat_tujuan text NOT NULL,
    denda integer NOT NULL,
    ongkir integer NOT NULL,
    total_biaya integer NOT NULL,
    tgl_bayar_sewa date NOT NULL,
    tgl_jatuh_tempo date NOT NULL,
    kode_tenor character varying(10) NOT NULL,
    id_pelanggan character varying(10) NOT NULL
);
    DROP TABLE sipelu.biaya_sewa;
       sipelu         anggardhanoano    false    4            �            1259    28431    django_admin_log    TABLE     �  CREATE TABLE sipelu.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);
 $   DROP TABLE sipelu.django_admin_log;
       sipelu         anggardhanoano    false    4            �            1259    28429    django_admin_log_id_seq    SEQUENCE     �   CREATE SEQUENCE sipelu.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE sipelu.django_admin_log_id_seq;
       sipelu       anggardhanoano    false    4    244            �           0    0    django_admin_log_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE sipelu.django_admin_log_id_seq OWNED BY sipelu.django_admin_log.id;
            sipelu       anggardhanoano    false    243            �            1259    28317    django_content_type    TABLE     �   CREATE TABLE sipelu.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);
 '   DROP TABLE sipelu.django_content_type;
       sipelu         anggardhanoano    false    4            �            1259    28315    django_content_type_id_seq    SEQUENCE     �   CREATE SEQUENCE sipelu.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE sipelu.django_content_type_id_seq;
       sipelu       anggardhanoano    false    230    4            �           0    0    django_content_type_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE sipelu.django_content_type_id_seq OWNED BY sipelu.django_content_type.id;
            sipelu       anggardhanoano    false    229            �            1259    28306    django_migrations    TABLE     �   CREATE TABLE sipelu.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
 %   DROP TABLE sipelu.django_migrations;
       sipelu         anggardhanoano    false    4            �            1259    28304    django_migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE sipelu.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE sipelu.django_migrations_id_seq;
       sipelu       anggardhanoano    false    4    228            �           0    0    django_migrations_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE sipelu.django_migrations_id_seq OWNED BY sipelu.django_migrations.id;
            sipelu       anggardhanoano    false    227            �            1259    28462    django_session    TABLE     �   CREATE TABLE sipelu.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);
 "   DROP TABLE sipelu.django_session;
       sipelu         anggardhanoano    false    4            �            1259    28007    fee    TABLE     �   CREATE TABLE sipelu.fee (
    tgl_terima character varying(10) NOT NULL,
    jumlah integer NOT NULL,
    id_pelanggan character varying(10) NOT NULL
);
    DROP TABLE sipelu.fee;
       sipelu         anggardhanoano    false    4            �            1259    28010    lukisan    TABLE       CREATE TABLE sipelu.lukisan (
    id_lukisan character varying(10) NOT NULL,
    nama_lukisan character varying(255) NOT NULL,
    tema_lukisan text NOT NULL,
    harga integer,
    tahun character varying(5) NOT NULL,
    id_pelanggan character varying(10) NOT NULL
);
    DROP TABLE sipelu.lukisan;
       sipelu         anggardhanoano    false    4            �            1259    28016 
   mitra_bank    TABLE        CREATE TABLE sipelu.mitra_bank (
    kode_bank character varying(5) NOT NULL,
    nama_bank character varying(255) NOT NULL
);
    DROP TABLE sipelu.mitra_bank;
       sipelu         anggardhanoano    false    4            �            1259    28019 	   pelanggan    TABLE     �  CREATE TABLE sipelu.pelanggan (
    id_pelanggan character varying(10) NOT NULL,
    no_ktp character varying(20) NOT NULL,
    nama character varying(255) NOT NULL,
    alamat text NOT NULL,
    tempat_tgl_lahir date NOT NULL,
    no_hp character varying(15) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(100) DEFAULT '-'::character varying NOT NULL
);
    DROP TABLE sipelu.pelanggan;
       sipelu         anggardhanoano    false    4            �            1259    28025    pemilik    TABLE     �   CREATE TABLE sipelu.pemilik (
    id_pelanggan character varying(10) NOT NULL,
    no_rek character varying(25) NOT NULL,
    nama_bank character varying(255) NOT NULL
);
    DROP TABLE sipelu.pemilik;
       sipelu         anggardhanoano    false    4            �            1259    28028    penyewa    TABLE     �   CREATE TABLE sipelu.penyewa (
    id_pelanggan character varying(10) NOT NULL,
    pekerjaan character varying(255) NOT NULL,
    kategori character varying(10) NOT NULL
);
    DROP TABLE sipelu.penyewa;
       sipelu         anggardhanoano    false    4            �            1259    28031    staff    TABLE     ;  CREATE TABLE sipelu.staff (
    no_staff character varying(10) NOT NULL,
    nama character varying(255) NOT NULL,
    tempat_tgl_lahir date NOT NULL,
    no_hp character varying(15) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(100) DEFAULT '-'::character varying NOT NULL
);
    DROP TABLE sipelu.staff;
       sipelu         anggardhanoano    false    4            �            1259    28037    staff_lulusan    TABLE     �   CREATE TABLE sipelu.staff_lulusan (
    no_staff character varying(10) NOT NULL,
    lulusan_staff character varying(255) NOT NULL
);
 !   DROP TABLE sipelu.staff_lulusan;
       sipelu         anggardhanoano    false    4            �            1259    28040    tenor_bayar    TABLE     �   CREATE TABLE sipelu.tenor_bayar (
    kode_tenor character varying(10) NOT NULL,
    tenor_pilihan character varying(255) NOT NULL,
    kode_bank character varying(5) NOT NULL
);
    DROP TABLE sipelu.tenor_bayar;
       sipelu         anggardhanoano    false    4            �            1259    28043    transaksi_sewa    TABLE     /  CREATE TABLE sipelu.transaksi_sewa (
    tgl_sewa date NOT NULL,
    tgl_kembali date NOT NULL,
    banyaknya integer NOT NULL,
    lama_sewa integer NOT NULL,
    no_staff character varying(10) NOT NULL,
    id_pelanggan character varying(10) NOT NULL,
    id_lukisan character varying(10) NOT NULL
);
 "   DROP TABLE sipelu.transaksi_sewa;
       sipelu         anggardhanoano    false    4            �           2604    28161    auth_group id    DEFAULT     n   ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);
 <   ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
       public       anggardhanoano    false    214    215    215            �           2604    28171    auth_group_permissions id    DEFAULT     �   ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);
 H   ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
       public       anggardhanoano    false    217    216    217            �           2604    28153    auth_permission id    DEFAULT     x   ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);
 A   ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
       public       anggardhanoano    false    213    212    213            �           2604    28179    auth_user id    DEFAULT     l   ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);
 ;   ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
       public       anggardhanoano    false    218    219    219            �           2604    28189    auth_user_groups id    DEFAULT     z   ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);
 B   ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
       public       anggardhanoano    false    221    220    221            �           2604    28197    auth_user_user_permissions id    DEFAULT     �   ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);
 L   ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
       public       anggardhanoano    false    222    223    223            �           2604    28257    django_admin_log id    DEFAULT     z   ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);
 B   ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
       public       anggardhanoano    false    225    224    225            �           2604    28143    django_content_type id    DEFAULT     �   ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);
 E   ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
       public       anggardhanoano    false    210    211    211            �           2604    28132    django_migrations id    DEFAULT     |   ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);
 C   ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
       public       anggardhanoano    false    209    208    209            �           2604    28338    auth_group id    DEFAULT     n   ALTER TABLE ONLY sipelu.auth_group ALTER COLUMN id SET DEFAULT nextval('sipelu.auth_group_id_seq'::regclass);
 <   ALTER TABLE sipelu.auth_group ALTER COLUMN id DROP DEFAULT;
       sipelu       anggardhanoano    false    233    234    234            �           2604    28348    auth_group_permissions id    DEFAULT     �   ALTER TABLE ONLY sipelu.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('sipelu.auth_group_permissions_id_seq'::regclass);
 H   ALTER TABLE sipelu.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
       sipelu       anggardhanoano    false    235    236    236            �           2604    28330    auth_permission id    DEFAULT     x   ALTER TABLE ONLY sipelu.auth_permission ALTER COLUMN id SET DEFAULT nextval('sipelu.auth_permission_id_seq'::regclass);
 A   ALTER TABLE sipelu.auth_permission ALTER COLUMN id DROP DEFAULT;
       sipelu       anggardhanoano    false    231    232    232            �           2604    28356    auth_user id    DEFAULT     l   ALTER TABLE ONLY sipelu.auth_user ALTER COLUMN id SET DEFAULT nextval('sipelu.auth_user_id_seq'::regclass);
 ;   ALTER TABLE sipelu.auth_user ALTER COLUMN id DROP DEFAULT;
       sipelu       anggardhanoano    false    238    237    238            �           2604    28366    auth_user_groups id    DEFAULT     z   ALTER TABLE ONLY sipelu.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('sipelu.auth_user_groups_id_seq'::regclass);
 B   ALTER TABLE sipelu.auth_user_groups ALTER COLUMN id DROP DEFAULT;
       sipelu       anggardhanoano    false    240    239    240            �           2604    28374    auth_user_user_permissions id    DEFAULT     �   ALTER TABLE ONLY sipelu.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('sipelu.auth_user_user_permissions_id_seq'::regclass);
 L   ALTER TABLE sipelu.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
       sipelu       anggardhanoano    false    242    241    242            �           2604    28434    django_admin_log id    DEFAULT     z   ALTER TABLE ONLY sipelu.django_admin_log ALTER COLUMN id SET DEFAULT nextval('sipelu.django_admin_log_id_seq'::regclass);
 B   ALTER TABLE sipelu.django_admin_log ALTER COLUMN id DROP DEFAULT;
       sipelu       anggardhanoano    false    243    244    244            �           2604    28320    django_content_type id    DEFAULT     �   ALTER TABLE ONLY sipelu.django_content_type ALTER COLUMN id SET DEFAULT nextval('sipelu.django_content_type_id_seq'::regclass);
 E   ALTER TABLE sipelu.django_content_type ALTER COLUMN id DROP DEFAULT;
       sipelu       anggardhanoano    false    229    230    230            �           2604    28309    django_migrations id    DEFAULT     |   ALTER TABLE ONLY sipelu.django_migrations ALTER COLUMN id SET DEFAULT nextval('sipelu.django_migrations_id_seq'::regclass);
 C   ALTER TABLE sipelu.django_migrations ALTER COLUMN id DROP DEFAULT;
       sipelu       anggardhanoano    false    228    227    228            �          0    28158 
   auth_group 
   TABLE DATA               .   COPY public.auth_group (id, name) FROM stdin;
    public       anggardhanoano    false    215   �\      �          0    28168    auth_group_permissions 
   TABLE DATA               M   COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
    public       anggardhanoano    false    217   �\      �          0    28150    auth_permission 
   TABLE DATA               N   COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
    public       anggardhanoano    false    213   �\      �          0    28176 	   auth_user 
   TABLE DATA               �   COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
    public       anggardhanoano    false    219   ^      �          0    28186    auth_user_groups 
   TABLE DATA               A   COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
    public       anggardhanoano    false    221   "^      �          0    28194    auth_user_user_permissions 
   TABLE DATA               P   COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
    public       anggardhanoano    false    223   ?^      �          0    28254    django_admin_log 
   TABLE DATA               �   COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
    public       anggardhanoano    false    225   \^      �          0    28140    django_content_type 
   TABLE DATA               C   COPY public.django_content_type (id, app_label, model) FROM stdin;
    public       anggardhanoano    false    211   y^      �          0    28129    django_migrations 
   TABLE DATA               C   COPY public.django_migrations (id, app, name, applied) FROM stdin;
    public       anggardhanoano    false    209   �^      �          0    28285    django_session 
   TABLE DATA               P   COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
    public       anggardhanoano    false    226   �`      �          0    28335 
   auth_group 
   TABLE DATA               .   COPY sipelu.auth_group (id, name) FROM stdin;
    sipelu       anggardhanoano    false    234   �`      �          0    28345    auth_group_permissions 
   TABLE DATA               M   COPY sipelu.auth_group_permissions (id, group_id, permission_id) FROM stdin;
    sipelu       anggardhanoano    false    236   �`      �          0    28327    auth_permission 
   TABLE DATA               N   COPY sipelu.auth_permission (id, name, content_type_id, codename) FROM stdin;
    sipelu       anggardhanoano    false    232   �`      �          0    28353 	   auth_user 
   TABLE DATA               �   COPY sipelu.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
    sipelu       anggardhanoano    false    238   	b      �          0    28363    auth_user_groups 
   TABLE DATA               A   COPY sipelu.auth_user_groups (id, user_id, group_id) FROM stdin;
    sipelu       anggardhanoano    false    240   &b      �          0    28371    auth_user_user_permissions 
   TABLE DATA               P   COPY sipelu.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
    sipelu       anggardhanoano    false    242   Cb      �          0    28001 
   biaya_sewa 
   TABLE DATA               �   COPY sipelu.biaya_sewa (tgl_sewa, biaya_sewa_lukisan, alamat_tujuan, denda, ongkir, total_biaya, tgl_bayar_sewa, tgl_jatuh_tempo, kode_tenor, id_pelanggan) FROM stdin;
    sipelu       anggardhanoano    false    197   `b      �          0    28431    django_admin_log 
   TABLE DATA               �   COPY sipelu.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
    sipelu       anggardhanoano    false    244   >e      �          0    28317    django_content_type 
   TABLE DATA               C   COPY sipelu.django_content_type (id, app_label, model) FROM stdin;
    sipelu       anggardhanoano    false    230   [e      �          0    28306    django_migrations 
   TABLE DATA               C   COPY sipelu.django_migrations (id, app, name, applied) FROM stdin;
    sipelu       anggardhanoano    false    228   �e      �          0    28462    django_session 
   TABLE DATA               P   COPY sipelu.django_session (session_key, session_data, expire_date) FROM stdin;
    sipelu       anggardhanoano    false    245   eg      �          0    28007    fee 
   TABLE DATA               ?   COPY sipelu.fee (tgl_terima, jumlah, id_pelanggan) FROM stdin;
    sipelu       anggardhanoano    false    198   �u      �          0    28010    lukisan 
   TABLE DATA               e   COPY sipelu.lukisan (id_lukisan, nama_lukisan, tema_lukisan, harga, tahun, id_pelanggan) FROM stdin;
    sipelu       anggardhanoano    false    199   �v      �          0    28016 
   mitra_bank 
   TABLE DATA               :   COPY sipelu.mitra_bank (kode_bank, nama_bank) FROM stdin;
    sipelu       anggardhanoano    false    200   my      �          0    28019 	   pelanggan 
   TABLE DATA               q   COPY sipelu.pelanggan (id_pelanggan, no_ktp, nama, alamat, tempat_tgl_lahir, no_hp, email, password) FROM stdin;
    sipelu       anggardhanoano    false    201   �y      �          0    28025    pemilik 
   TABLE DATA               B   COPY sipelu.pemilik (id_pelanggan, no_rek, nama_bank) FROM stdin;
    sipelu       anggardhanoano    false    202   �      �          0    28028    penyewa 
   TABLE DATA               D   COPY sipelu.penyewa (id_pelanggan, pekerjaan, kategori) FROM stdin;
    sipelu       anggardhanoano    false    203   �      �          0    28031    staff 
   TABLE DATA               Y   COPY sipelu.staff (no_staff, nama, tempat_tgl_lahir, no_hp, email, password) FROM stdin;
    sipelu       anggardhanoano    false    204   �      �          0    28037    staff_lulusan 
   TABLE DATA               @   COPY sipelu.staff_lulusan (no_staff, lulusan_staff) FROM stdin;
    sipelu       anggardhanoano    false    205         �          0    28040    tenor_bayar 
   TABLE DATA               K   COPY sipelu.tenor_bayar (kode_tenor, tenor_pilihan, kode_bank) FROM stdin;
    sipelu       anggardhanoano    false    206   )�      �          0    28043    transaksi_sewa 
   TABLE DATA               y   COPY sipelu.transaksi_sewa (tgl_sewa, tgl_kembali, banyaknya, lama_sewa, no_staff, id_pelanggan, id_lukisan) FROM stdin;
    sipelu       anggardhanoano    false    207   s�      �           0    0    auth_group_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);
            public       anggardhanoano    false    214            �           0    0    auth_group_permissions_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);
            public       anggardhanoano    false    216            �           0    0    auth_permission_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.auth_permission_id_seq', 24, true);
            public       anggardhanoano    false    212                        0    0    auth_user_groups_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);
            public       anggardhanoano    false    220                       0    0    auth_user_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.auth_user_id_seq', 1, false);
            public       anggardhanoano    false    218                       0    0 !   auth_user_user_permissions_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);
            public       anggardhanoano    false    222                       0    0    django_admin_log_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);
            public       anggardhanoano    false    224                       0    0    django_content_type_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.django_content_type_id_seq', 6, true);
            public       anggardhanoano    false    210                       0    0    django_migrations_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.django_migrations_id_seq', 17, true);
            public       anggardhanoano    false    208                       0    0    auth_group_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('sipelu.auth_group_id_seq', 1, false);
            sipelu       anggardhanoano    false    233                       0    0    auth_group_permissions_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('sipelu.auth_group_permissions_id_seq', 1, false);
            sipelu       anggardhanoano    false    235                       0    0    auth_permission_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('sipelu.auth_permission_id_seq', 24, true);
            sipelu       anggardhanoano    false    231            	           0    0    auth_user_groups_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('sipelu.auth_user_groups_id_seq', 1, false);
            sipelu       anggardhanoano    false    239            
           0    0    auth_user_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('sipelu.auth_user_id_seq', 1, false);
            sipelu       anggardhanoano    false    237                       0    0 !   auth_user_user_permissions_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('sipelu.auth_user_user_permissions_id_seq', 1, false);
            sipelu       anggardhanoano    false    241                       0    0    django_admin_log_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('sipelu.django_admin_log_id_seq', 1, false);
            sipelu       anggardhanoano    false    243                       0    0    django_content_type_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('sipelu.django_content_type_id_seq', 6, true);
            sipelu       anggardhanoano    false    229                       0    0    django_migrations_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('sipelu.django_migrations_id_seq', 17, true);
            sipelu       anggardhanoano    false    227            �           2606    28283    auth_group auth_group_name_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);
 H   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
       public         anggardhanoano    false    215            �           2606    28220 R   auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);
 |   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
       public         anggardhanoano    false    217    217            �           2606    28173 2   auth_group_permissions auth_group_permissions_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
       public         anggardhanoano    false    217            �           2606    28163    auth_group auth_group_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
       public         anggardhanoano    false    215            �           2606    28206 F   auth_permission auth_permission_content_type_id_codename_01ab375a_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);
 p   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
       public         anggardhanoano    false    213    213            �           2606    28155 $   auth_permission auth_permission_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
       public         anggardhanoano    false    213            �           2606    28191 &   auth_user_groups auth_user_groups_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
       public         anggardhanoano    false    221            �           2606    28235 @   auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);
 j   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
       public         anggardhanoano    false    221    221            �           2606    28181    auth_user auth_user_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
       public         anggardhanoano    false    219            �           2606    28199 :   auth_user_user_permissions auth_user_user_permissions_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
       public         anggardhanoano    false    223            �           2606    28249 Y   auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
       public         anggardhanoano    false    223    223            �           2606    28277     auth_user auth_user_username_key 
   CONSTRAINT     _   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);
 J   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
       public         anggardhanoano    false    219            �           2606    28263 &   django_admin_log django_admin_log_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
       public         anggardhanoano    false    225            �           2606    28147 E   django_content_type django_content_type_app_label_model_76bd3d3b_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);
 o   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
       public         anggardhanoano    false    211    211            �           2606    28145 ,   django_content_type django_content_type_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
       public         anggardhanoano    false    211            �           2606    28137 (   django_migrations django_migrations_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
       public         anggardhanoano    false    209            �           2606    28292 "   django_session django_session_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);
 L   ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
       public         anggardhanoano    false    226            �           2606    28460    auth_group auth_group_name_key 
   CONSTRAINT     Y   ALTER TABLE ONLY sipelu.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);
 H   ALTER TABLE ONLY sipelu.auth_group DROP CONSTRAINT auth_group_name_key;
       sipelu         anggardhanoano    false    234            �           2606    28397 R   auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY sipelu.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);
 |   ALTER TABLE ONLY sipelu.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
       sipelu         anggardhanoano    false    236    236            �           2606    28350 2   auth_group_permissions auth_group_permissions_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY sipelu.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY sipelu.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
       sipelu         anggardhanoano    false    236            �           2606    28340    auth_group auth_group_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY sipelu.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY sipelu.auth_group DROP CONSTRAINT auth_group_pkey;
       sipelu         anggardhanoano    false    234            �           2606    28383 F   auth_permission auth_permission_content_type_id_codename_01ab375a_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY sipelu.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);
 p   ALTER TABLE ONLY sipelu.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
       sipelu         anggardhanoano    false    232    232            �           2606    28332 $   auth_permission auth_permission_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY sipelu.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY sipelu.auth_permission DROP CONSTRAINT auth_permission_pkey;
       sipelu         anggardhanoano    false    232                       2606    28368 &   auth_user_groups auth_user_groups_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY sipelu.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY sipelu.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
       sipelu         anggardhanoano    false    240            	           2606    28412 @   auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY sipelu.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);
 j   ALTER TABLE ONLY sipelu.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
       sipelu         anggardhanoano    false    240    240                        2606    28358    auth_user auth_user_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY sipelu.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY sipelu.auth_user DROP CONSTRAINT auth_user_pkey;
       sipelu         anggardhanoano    false    238                       2606    28376 :   auth_user_user_permissions auth_user_user_permissions_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY sipelu.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY sipelu.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
       sipelu         anggardhanoano    false    242                       2606    28426 Y   auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY sipelu.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);
 �   ALTER TABLE ONLY sipelu.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
       sipelu         anggardhanoano    false    242    242                       2606    28454     auth_user auth_user_username_key 
   CONSTRAINT     _   ALTER TABLE ONLY sipelu.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);
 J   ALTER TABLE ONLY sipelu.auth_user DROP CONSTRAINT auth_user_username_key;
       sipelu         anggardhanoano    false    238            �           2606    28047    biaya_sewa biaya_sewa_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY sipelu.biaya_sewa
    ADD CONSTRAINT biaya_sewa_pkey PRIMARY KEY (tgl_sewa);
 D   ALTER TABLE ONLY sipelu.biaya_sewa DROP CONSTRAINT biaya_sewa_pkey;
       sipelu         anggardhanoano    false    197                       2606    28440 &   django_admin_log django_admin_log_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY sipelu.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY sipelu.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
       sipelu         anggardhanoano    false    244            �           2606    28324 E   django_content_type django_content_type_app_label_model_76bd3d3b_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY sipelu.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);
 o   ALTER TABLE ONLY sipelu.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
       sipelu         anggardhanoano    false    230    230            �           2606    28322 ,   django_content_type django_content_type_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY sipelu.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY sipelu.django_content_type DROP CONSTRAINT django_content_type_pkey;
       sipelu         anggardhanoano    false    230            �           2606    28314 (   django_migrations django_migrations_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY sipelu.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY sipelu.django_migrations DROP CONSTRAINT django_migrations_pkey;
       sipelu         anggardhanoano    false    228                       2606    28469 "   django_session django_session_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY sipelu.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);
 L   ALTER TABLE ONLY sipelu.django_session DROP CONSTRAINT django_session_pkey;
       sipelu         anggardhanoano    false    245            �           2606    28049    fee fee_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY sipelu.fee
    ADD CONSTRAINT fee_pkey PRIMARY KEY (tgl_terima);
 6   ALTER TABLE ONLY sipelu.fee DROP CONSTRAINT fee_pkey;
       sipelu         anggardhanoano    false    198            �           2606    28051    lukisan lukisan_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY sipelu.lukisan
    ADD CONSTRAINT lukisan_pkey PRIMARY KEY (id_lukisan);
 >   ALTER TABLE ONLY sipelu.lukisan DROP CONSTRAINT lukisan_pkey;
       sipelu         anggardhanoano    false    199            �           2606    28053    mitra_bank mitra_bank_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY sipelu.mitra_bank
    ADD CONSTRAINT mitra_bank_pkey PRIMARY KEY (kode_bank);
 D   ALTER TABLE ONLY sipelu.mitra_bank DROP CONSTRAINT mitra_bank_pkey;
       sipelu         anggardhanoano    false    200            �           2606    28055    pelanggan pelanggan_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY sipelu.pelanggan
    ADD CONSTRAINT pelanggan_pkey PRIMARY KEY (id_pelanggan);
 B   ALTER TABLE ONLY sipelu.pelanggan DROP CONSTRAINT pelanggan_pkey;
       sipelu         anggardhanoano    false    201            �           2606    28057    pemilik pemilik_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY sipelu.pemilik
    ADD CONSTRAINT pemilik_pkey PRIMARY KEY (id_pelanggan);
 >   ALTER TABLE ONLY sipelu.pemilik DROP CONSTRAINT pemilik_pkey;
       sipelu         anggardhanoano    false    202            �           2606    28059    penyewa penyewa_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY sipelu.penyewa
    ADD CONSTRAINT penyewa_pkey PRIMARY KEY (id_pelanggan);
 >   ALTER TABLE ONLY sipelu.penyewa DROP CONSTRAINT penyewa_pkey;
       sipelu         anggardhanoano    false    203            �           2606    28061     staff_lulusan staff_lulusan_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY sipelu.staff_lulusan
    ADD CONSTRAINT staff_lulusan_pkey PRIMARY KEY (no_staff);
 J   ALTER TABLE ONLY sipelu.staff_lulusan DROP CONSTRAINT staff_lulusan_pkey;
       sipelu         anggardhanoano    false    205            �           2606    28063    staff staff_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY sipelu.staff
    ADD CONSTRAINT staff_pkey PRIMARY KEY (no_staff);
 :   ALTER TABLE ONLY sipelu.staff DROP CONSTRAINT staff_pkey;
       sipelu         anggardhanoano    false    204            �           2606    28065    tenor_bayar tenor_bayar_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY sipelu.tenor_bayar
    ADD CONSTRAINT tenor_bayar_pkey PRIMARY KEY (kode_tenor);
 F   ALTER TABLE ONLY sipelu.tenor_bayar DROP CONSTRAINT tenor_bayar_pkey;
       sipelu         anggardhanoano    false    206            �           2606    28067 "   transaksi_sewa transaksi_sewa_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY sipelu.transaksi_sewa
    ADD CONSTRAINT transaksi_sewa_pkey PRIMARY KEY (tgl_sewa);
 L   ALTER TABLE ONLY sipelu.transaksi_sewa DROP CONSTRAINT transaksi_sewa_pkey;
       sipelu         anggardhanoano    false    207            �           1259    28284    auth_group_name_a6ea08ec_like    INDEX     h   CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);
 1   DROP INDEX public.auth_group_name_a6ea08ec_like;
       public         anggardhanoano    false    215            �           1259    28221 (   auth_group_permissions_group_id_b120cbf9    INDEX     o   CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);
 <   DROP INDEX public.auth_group_permissions_group_id_b120cbf9;
       public         anggardhanoano    false    217            �           1259    28222 -   auth_group_permissions_permission_id_84c5c92e    INDEX     y   CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);
 A   DROP INDEX public.auth_group_permissions_permission_id_84c5c92e;
       public         anggardhanoano    false    217            �           1259    28207 (   auth_permission_content_type_id_2f476e4b    INDEX     o   CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);
 <   DROP INDEX public.auth_permission_content_type_id_2f476e4b;
       public         anggardhanoano    false    213            �           1259    28237 "   auth_user_groups_group_id_97559544    INDEX     c   CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);
 6   DROP INDEX public.auth_user_groups_group_id_97559544;
       public         anggardhanoano    false    221            �           1259    28236 !   auth_user_groups_user_id_6a12ed8b    INDEX     a   CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);
 5   DROP INDEX public.auth_user_groups_user_id_6a12ed8b;
       public         anggardhanoano    false    221            �           1259    28251 1   auth_user_user_permissions_permission_id_1fbb5f2c    INDEX     �   CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);
 E   DROP INDEX public.auth_user_user_permissions_permission_id_1fbb5f2c;
       public         anggardhanoano    false    223            �           1259    28250 +   auth_user_user_permissions_user_id_a95ead1b    INDEX     u   CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);
 ?   DROP INDEX public.auth_user_user_permissions_user_id_a95ead1b;
       public         anggardhanoano    false    223            �           1259    28278     auth_user_username_6821ab7c_like    INDEX     n   CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);
 4   DROP INDEX public.auth_user_username_6821ab7c_like;
       public         anggardhanoano    false    219            �           1259    28274 )   django_admin_log_content_type_id_c4bce8eb    INDEX     q   CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);
 =   DROP INDEX public.django_admin_log_content_type_id_c4bce8eb;
       public         anggardhanoano    false    225            �           1259    28275 !   django_admin_log_user_id_c564eba6    INDEX     a   CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);
 5   DROP INDEX public.django_admin_log_user_id_c564eba6;
       public         anggardhanoano    false    225            �           1259    28294 #   django_session_expire_date_a5c62663    INDEX     e   CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);
 7   DROP INDEX public.django_session_expire_date_a5c62663;
       public         anggardhanoano    false    226            �           1259    28293 (   django_session_session_key_c0390e0f_like    INDEX     ~   CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);
 <   DROP INDEX public.django_session_session_key_c0390e0f_like;
       public         anggardhanoano    false    226            �           1259    28461    auth_group_name_a6ea08ec_like    INDEX     h   CREATE INDEX auth_group_name_a6ea08ec_like ON sipelu.auth_group USING btree (name varchar_pattern_ops);
 1   DROP INDEX sipelu.auth_group_name_a6ea08ec_like;
       sipelu         anggardhanoano    false    234            �           1259    28398 (   auth_group_permissions_group_id_b120cbf9    INDEX     o   CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON sipelu.auth_group_permissions USING btree (group_id);
 <   DROP INDEX sipelu.auth_group_permissions_group_id_b120cbf9;
       sipelu         anggardhanoano    false    236            �           1259    28399 -   auth_group_permissions_permission_id_84c5c92e    INDEX     y   CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON sipelu.auth_group_permissions USING btree (permission_id);
 A   DROP INDEX sipelu.auth_group_permissions_permission_id_84c5c92e;
       sipelu         anggardhanoano    false    236            �           1259    28384 (   auth_permission_content_type_id_2f476e4b    INDEX     o   CREATE INDEX auth_permission_content_type_id_2f476e4b ON sipelu.auth_permission USING btree (content_type_id);
 <   DROP INDEX sipelu.auth_permission_content_type_id_2f476e4b;
       sipelu         anggardhanoano    false    232                       1259    28414 "   auth_user_groups_group_id_97559544    INDEX     c   CREATE INDEX auth_user_groups_group_id_97559544 ON sipelu.auth_user_groups USING btree (group_id);
 6   DROP INDEX sipelu.auth_user_groups_group_id_97559544;
       sipelu         anggardhanoano    false    240                       1259    28413 !   auth_user_groups_user_id_6a12ed8b    INDEX     a   CREATE INDEX auth_user_groups_user_id_6a12ed8b ON sipelu.auth_user_groups USING btree (user_id);
 5   DROP INDEX sipelu.auth_user_groups_user_id_6a12ed8b;
       sipelu         anggardhanoano    false    240            
           1259    28428 1   auth_user_user_permissions_permission_id_1fbb5f2c    INDEX     �   CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON sipelu.auth_user_user_permissions USING btree (permission_id);
 E   DROP INDEX sipelu.auth_user_user_permissions_permission_id_1fbb5f2c;
       sipelu         anggardhanoano    false    242                       1259    28427 +   auth_user_user_permissions_user_id_a95ead1b    INDEX     u   CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON sipelu.auth_user_user_permissions USING btree (user_id);
 ?   DROP INDEX sipelu.auth_user_user_permissions_user_id_a95ead1b;
       sipelu         anggardhanoano    false    242                       1259    28455     auth_user_username_6821ab7c_like    INDEX     n   CREATE INDEX auth_user_username_6821ab7c_like ON sipelu.auth_user USING btree (username varchar_pattern_ops);
 4   DROP INDEX sipelu.auth_user_username_6821ab7c_like;
       sipelu         anggardhanoano    false    238                       1259    28451 )   django_admin_log_content_type_id_c4bce8eb    INDEX     q   CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON sipelu.django_admin_log USING btree (content_type_id);
 =   DROP INDEX sipelu.django_admin_log_content_type_id_c4bce8eb;
       sipelu         anggardhanoano    false    244                       1259    28452 !   django_admin_log_user_id_c564eba6    INDEX     a   CREATE INDEX django_admin_log_user_id_c564eba6 ON sipelu.django_admin_log USING btree (user_id);
 5   DROP INDEX sipelu.django_admin_log_user_id_c564eba6;
       sipelu         anggardhanoano    false    244                       1259    28471 #   django_session_expire_date_a5c62663    INDEX     e   CREATE INDEX django_session_expire_date_a5c62663 ON sipelu.django_session USING btree (expire_date);
 7   DROP INDEX sipelu.django_session_expire_date_a5c62663;
       sipelu         anggardhanoano    false    245                       1259    28470 (   django_session_session_key_c0390e0f_like    INDEX     ~   CREATE INDEX django_session_session_key_c0390e0f_like ON sipelu.django_session USING btree (session_key varchar_pattern_ops);
 <   DROP INDEX sipelu.django_session_session_key_c0390e0f_like;
       sipelu         anggardhanoano    false    245            8           2620    28068     transaksi_sewa cek_durasi_pinjam    TRIGGER     �   CREATE TRIGGER cek_durasi_pinjam BEFORE INSERT OR UPDATE ON sipelu.transaksi_sewa FOR EACH ROW EXECUTE PROCEDURE sipelu.validasi_durasi();
 9   DROP TRIGGER cek_durasi_pinjam ON sipelu.transaksi_sewa;
       sipelu       anggardhanoano    false    207    260            7           2620    28069    tenor_bayar cek_tenor_bayar    TRIGGER     �   CREATE TRIGGER cek_tenor_bayar BEFORE INSERT OR UPDATE ON sipelu.tenor_bayar FOR EACH ROW EXECUTE PROCEDURE sipelu.validasi_tenor();
 4   DROP TRIGGER cek_tenor_bayar ON sipelu.tenor_bayar;
       sipelu       anggardhanoano    false    206    261            5           2620    28070    biaya_sewa set_diskon    TRIGGER     ~   CREATE TRIGGER set_diskon BEFORE INSERT OR UPDATE ON sipelu.biaya_sewa FOR EACH ROW EXECUTE PROCEDURE sipelu.hitung_diskon();
 .   DROP TRIGGER set_diskon ON sipelu.biaya_sewa;
       sipelu       anggardhanoano    false    246    197            6           2620    28071    biaya_sewa set_fee    TRIGGER     w   CREATE TRIGGER set_fee AFTER INSERT OR UPDATE ON sipelu.biaya_sewa FOR EACH ROW EXECUTE PROCEDURE sipelu.hitung_fee();
 +   DROP TRIGGER set_fee ON sipelu.biaya_sewa;
       sipelu       anggardhanoano    false    259    197            %           2606    28214 O   auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 y   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
       public       anggardhanoano    false    217    3012    213            $           2606    28209 P   auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
       public       anggardhanoano    false    217    215    3017            #           2606    28200 E   auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 o   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
       public       anggardhanoano    false    213    211    3007            '           2606    28229 D   auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 n   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
       public       anggardhanoano    false    221    3017    215            &           2606    28224 B   auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
       public       anggardhanoano    false    221    219    3025            )           2606    28243 S   auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 }   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
       public       anggardhanoano    false    223    3012    213            (           2606    28238 V   auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
       public       anggardhanoano    false    219    223    3025            *           2606    28264 G   django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 q   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
       public       anggardhanoano    false    3007    211    225            +           2606    28269 B   django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
       public       anggardhanoano    false    225    3025    219            .           2606    28391 O   auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES sipelu.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 y   ALTER TABLE ONLY sipelu.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
       sipelu       anggardhanoano    false    232    3059    236            -           2606    28386 P   auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES sipelu.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY sipelu.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
       sipelu       anggardhanoano    false    236    3064    234            ,           2606    28377 E   auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES sipelu.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 o   ALTER TABLE ONLY sipelu.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
       sipelu       anggardhanoano    false    230    3054    232            0           2606    28406 D   auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES sipelu.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 n   ALTER TABLE ONLY sipelu.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
       sipelu       anggardhanoano    false    240    234    3064            /           2606    28401 B   auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES sipelu.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY sipelu.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
       sipelu       anggardhanoano    false    240    238    3072            2           2606    28420 S   auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES sipelu.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 }   ALTER TABLE ONLY sipelu.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
       sipelu       anggardhanoano    false    242    232    3059            1           2606    28415 V   auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES sipelu.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY sipelu.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
       sipelu       anggardhanoano    false    242    238    3072                       2606    28072 '   biaya_sewa biaya_sewa_id_pelanggan_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.biaya_sewa
    ADD CONSTRAINT biaya_sewa_id_pelanggan_fkey FOREIGN KEY (id_pelanggan) REFERENCES sipelu.pelanggan(id_pelanggan) ON UPDATE CASCADE ON DELETE CASCADE;
 Q   ALTER TABLE ONLY sipelu.biaya_sewa DROP CONSTRAINT biaya_sewa_id_pelanggan_fkey;
       sipelu       anggardhanoano    false    2989    197    201                       2606    28077 %   biaya_sewa biaya_sewa_kode_tenor_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.biaya_sewa
    ADD CONSTRAINT biaya_sewa_kode_tenor_fkey FOREIGN KEY (kode_tenor) REFERENCES sipelu.tenor_bayar(kode_tenor) ON UPDATE CASCADE ON DELETE CASCADE;
 O   ALTER TABLE ONLY sipelu.biaya_sewa DROP CONSTRAINT biaya_sewa_kode_tenor_fkey;
       sipelu       anggardhanoano    false    2999    206    197                       2606    28082 #   biaya_sewa biaya_sewa_tgl_sewa_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.biaya_sewa
    ADD CONSTRAINT biaya_sewa_tgl_sewa_fkey FOREIGN KEY (tgl_sewa) REFERENCES sipelu.transaksi_sewa(tgl_sewa) ON UPDATE CASCADE ON DELETE CASCADE;
 M   ALTER TABLE ONLY sipelu.biaya_sewa DROP CONSTRAINT biaya_sewa_tgl_sewa_fkey;
       sipelu       anggardhanoano    false    3001    197    207            3           2606    28441 G   django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES sipelu.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 q   ALTER TABLE ONLY sipelu.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
       sipelu       anggardhanoano    false    244    230    3054            4           2606    28446 B   django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES sipelu.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY sipelu.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
       sipelu       anggardhanoano    false    244    238    3072                       2606    28087    fee fee_id_pelanggan_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.fee
    ADD CONSTRAINT fee_id_pelanggan_fkey FOREIGN KEY (id_pelanggan) REFERENCES sipelu.pelanggan(id_pelanggan) ON UPDATE CASCADE ON DELETE CASCADE;
 C   ALTER TABLE ONLY sipelu.fee DROP CONSTRAINT fee_id_pelanggan_fkey;
       sipelu       anggardhanoano    false    198    2989    201                       2606    28092 !   lukisan lukisan_id_pelanggan_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.lukisan
    ADD CONSTRAINT lukisan_id_pelanggan_fkey FOREIGN KEY (id_pelanggan) REFERENCES sipelu.pelanggan(id_pelanggan) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY sipelu.lukisan DROP CONSTRAINT lukisan_id_pelanggan_fkey;
       sipelu       anggardhanoano    false    201    2989    199                       2606    28097 !   pemilik pemilik_id_pelanggan_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.pemilik
    ADD CONSTRAINT pemilik_id_pelanggan_fkey FOREIGN KEY (id_pelanggan) REFERENCES sipelu.pelanggan(id_pelanggan) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY sipelu.pemilik DROP CONSTRAINT pemilik_id_pelanggan_fkey;
       sipelu       anggardhanoano    false    2989    202    201                       2606    28102 !   penyewa penyewa_id_pelanggan_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.penyewa
    ADD CONSTRAINT penyewa_id_pelanggan_fkey FOREIGN KEY (id_pelanggan) REFERENCES sipelu.pelanggan(id_pelanggan) ON UPDATE CASCADE ON DELETE CASCADE;
 K   ALTER TABLE ONLY sipelu.penyewa DROP CONSTRAINT penyewa_id_pelanggan_fkey;
       sipelu       anggardhanoano    false    2989    203    201                       2606    28107 &   tenor_bayar tenor_bayar_kode_bank_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.tenor_bayar
    ADD CONSTRAINT tenor_bayar_kode_bank_fkey FOREIGN KEY (kode_bank) REFERENCES sipelu.mitra_bank(kode_bank) ON UPDATE CASCADE ON DELETE CASCADE;
 P   ALTER TABLE ONLY sipelu.tenor_bayar DROP CONSTRAINT tenor_bayar_kode_bank_fkey;
       sipelu       anggardhanoano    false    206    2987    200                        2606    28112 -   transaksi_sewa transaksi_sewa_id_lukisan_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.transaksi_sewa
    ADD CONSTRAINT transaksi_sewa_id_lukisan_fkey FOREIGN KEY (id_lukisan) REFERENCES sipelu.lukisan(id_lukisan) ON UPDATE CASCADE ON DELETE CASCADE;
 W   ALTER TABLE ONLY sipelu.transaksi_sewa DROP CONSTRAINT transaksi_sewa_id_lukisan_fkey;
       sipelu       anggardhanoano    false    207    199    2985            !           2606    28117 /   transaksi_sewa transaksi_sewa_id_pelanggan_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.transaksi_sewa
    ADD CONSTRAINT transaksi_sewa_id_pelanggan_fkey FOREIGN KEY (id_pelanggan) REFERENCES sipelu.pelanggan(id_pelanggan) ON UPDATE CASCADE ON DELETE CASCADE;
 Y   ALTER TABLE ONLY sipelu.transaksi_sewa DROP CONSTRAINT transaksi_sewa_id_pelanggan_fkey;
       sipelu       anggardhanoano    false    2989    207    201            "           2606    28122 +   transaksi_sewa transaksi_sewa_no_staff_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY sipelu.transaksi_sewa
    ADD CONSTRAINT transaksi_sewa_no_staff_fkey FOREIGN KEY (no_staff) REFERENCES sipelu.staff(no_staff) ON UPDATE CASCADE ON DELETE CASCADE;
 U   ALTER TABLE ONLY sipelu.transaksi_sewa DROP CONSTRAINT transaksi_sewa_no_staff_fkey;
       sipelu       anggardhanoano    false    204    2995    207            �      x������ � �      �      x������ � �      �     x�]�K��0��ur
N0j£�z�Q�B�b�:�h�����I�%��o��i�g�f��c�Y����G�<��ЂRq�0G��f�a����[�m��Le���4u}0�x`+� �	{�}�R��NT����v�����֝d�~O]D�l�	�����<,�����ӺB�[ZA)���T���qu�<�͏>��_��� ��m�.2�)H���:x�ޟ8�1WY��'���`:Y��;�LL�vf���x댰j'�k�2�      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   W   x�M�K
�0�y����M�P�C�.z{E��LOn�!љ='���\��
K�!'�o�k��JU̴�d�f���)?�~��pM^%�      �   �  x����n� ��{_1|�<�J%��d/�y��HEM/�����_�]S�m6c�nuəy ��,~|8s�0&���X��`����B PXQ*�`��[�-!gH��XJؽ���O��p��.����~����1(�JN�����N�Kr~�g3�˫w��@�R���K|��|vsl貣W����K^(��#����ٰ�KK�^/�]�v��%)'��bl=�=��.���%$(�j
{T�R��[���!�J n4fS����so�Q����h�����]h���u	���@�I����G+ofvW�|�u�6�bc4Sw��X1���ƍ�t��Y"x�;�&�/��ʎb�R��|PS���#���#����#�z����E��IA���!����[�������A���?��CK      �      x������ � �      �      x������ � �      �      x������ � �      �     x�]�K��0��ur
N0j£�z�Q�B�b�:�h�����I�%��o��i�g�f��c�Y����G�<��ЂRq�0G��f�a����[�m��Le���4u}0�x`+� �	{�}�R��NT����v�����֝d�~O]D�l�	�����<,�����ӺB�[ZA)���T���qu�<�͏>��_��� ��m�.2�)H���:x�ޟ8�1WY��'���`:Y��;�LL�vf���x댰j'�k�2�      �      x������ � �      �      x������ � �      �      x������ � �      �   �  x�}V�n�0}6_��V����hW+��*�)e!ID������ g� #a�3sf�`�|`��+�ه<�����JwtS5�I��������wtۇP�͏%c�lׯ��Y8�����,�kW��M��Et���Ǣ�})@�<���.�S7t�Yҟ��=��-��ظ�܇"0 2d(ϰ�7}m�kw32�<������D�H�.���\��t������yU@��20��¦��C�d��"�������9"CP�(m��Q�1�\V}�(��a1��HH �(S��)�K$�D�0++};��E;3�n�ٸ��9K�s���'��_�[��賅�Z�#��
ޫ�'���U� /t��5��ݹ1ޤ��ޤRy�I!�u����|4t������n-
,�28�-j~,�x�l��- 5��;�� )4*�qc}�I�-F�f��}���\z����h]���^�Y7�o�Y�1%�a�^������Rп�O���*���5��m~�բ��zwt)��8u� K)3�칡/�f[��f>�8 n�����Sq��b��К	2��sWRv�EB�+�9��F�dm^MCO��6~�(w$�գ�	e`�r���O����w�8�35�[���b�&�f,L�ȩ�0�7�����;�ؽl7���̑#��`�oq7��b��]�^99��'��j��q6������      �      x������ � �      �   W   x�M�K
�0�y����M�P�C�.z{E��LOn�!љ='���\��
K�!'�o�k��JU̴�d�f���)?�~��pM^%�      �   �  x���Mn� F��S�~�ů�YFB��8H�8����ġ[
#g��G���d��n��B�]l���|�����a|&��%��E�����ǈ0Zd���.Gr&#����G�r~ʅ��
f��F�����k�A��������9�/��E�8=�˫��l��b�����?t��j�UvԢg�GqB���V�%�j�lc,%�����rf��.J�˂��-���ں#
G���Z6
}���������	!b,@��N�T���������_Mh���m64�ջ*�����A��)�R޵�W�|���&�lb���G, Wi��k�@���:�@I�p�+����b�(@;j
~[����0��5+2j���0�}?#Q'��~��{K�������	��C}      �   �  x���n�ƶ��;O���-�HV	�Eܲd)!�L��p��8�OJ���4{w� `ÂE�V���Z�Ɓ0\0�/��Z4�T�w�%�Y�$��T���� �{dn�DGzl��Q��v��>6b7~<�6
�����t`��t���7���n�}��O@�/�����,![����PjX���EI[+�-UQ�q1�A����M/!\ +D��T� X�k3��Z<�Q�U�BǼLi�"�IC���z�@�z�NL��FlP#�F�=TK����R=���fk�����.�S�;x�����/�w�ӽѽ�ڿ|�V����um���1�s�bݘ��t��w���ey��林}Aa'?�\?��:���A#��������$�y]��2���}�JB�B�~5�s����v8���`����nog��0��0��#ڭ��d��=:<����ֽ��sa���Bg�3W��|�zoe��,Y��:�z��C���S���V_�W�
JA�?��˷����`XQ�]�����!Iî�k\�&��L������p���!�te�]3o��b��H	�.+��Jݪ-����aG�%����'v��VuQh�J�IJ�0�����>;u��B����I6���b�À���E\+��h��N�l��rm��O����w�q�vz|�Z�O��}g��|�Ir��;���V��N�����Gw�N��������j��΍q��;��3�L��Mn����d��%�?9s�z�{k�-���qu/��w���8'�{��	p�$�Ȝt�ǻlb���6��vtƋ>���摅�����������8ٿ{05'�w6���s�/������4jkA�s��6�����փ,���2��{q�N����̀v���n�Y��Gs�xI�Bň��G�������Qq��]�>H\D�r�0���� S��hA(bꔗv��"	�K�$���������q=3�����ꕩ���R Ü�%q���,�N'�)Q�`f��B�Dd��� �2�\۩�C����6I`E���(r�"��� �)G�`>1m�%�ƢW� �����/"�6q�]�I_iČ����[,��;�F������4�OH;�#m���c�͙���|O�`J��3o^Љ8�d���� ���s�N����Tʦ��̔��0�s�_zEfP�R�͇^��zf~��1XB�`*�|qEmj��i�z�K�L�>*XTVJkO̾��1��w��|�̛��{�c�Q7Z.!��(�z6;��A+g3�`ʯy3F�7OLպ�r��c����6 `9������c�`*}/@�%Uxe: �ҵ�:0�p�1+)�1`�q����Lo�)Z�@`uʛQ���(pH��U!<xE6�
�y�����gJ�~*T���h:�5�����J����y$���]eΑn�)[���P0�3'���v7�x���D>��M� �g��)�CX��J�{e*
�ƻاeO[փҧ�h��L��sf�7��-1_(���1��_�,*�$q�U"膒�j�&��g��)^ �$��2B_s$��+��L�����6���/�ڝ�� S�{�@����)�|?�x= uD�4v@ڜr h43�`��k 0�L�DØd�(r�a�D#t2TV�f}��~93����S��^F�fp��,h�5e���C#��@t��f��)�b]�@��'I�`�cWAXf񁧴�c�e=w�[@J�K�A�LS��G�$
��.{g���2�m�0!zםS�[`J�P�LU��L��C�G���G��C��c	�HC��ӛ`*�)^(�Cu
��b�ȚQx�M^E��Re������C3�?C~M� Xp�b4�S'�M^"�;�"�[�Y�P�U��b<3��l�r 
=�$�fy�G�t��ܚ��T�A�:�4�-0�K
*&���4UI8"�e-D,P����ػ�0��`
��x$�r�:-�`���rg��N����-0Er�� 
 �˧�c����1oTׁ��ɝ!I�^��h�-0�P������ŬmT��CT��Nh٪ U�͜LD�Oo�)YRe� '|:�Q��]EMV%c��VP��sE��|f����)A��*|�G��"w�R����<<^���$�k�p��7���� �L��O�A�}�OT)v�Z��)�"`U$��||��"��x��m�B!wڨJ�*H��-��{�Z����3���@�14��D�N���q�Q�Ra�����y]���kW]@R:�S\��R6�Ž�I�U��j�e#=p53��L�nG"�u|J_��N$�I%/oˮ	9l`�E�n���>&��B#5C�p'����%Om%Fm4���s�FFl��F#�a�ia�yg���N�Lry��i�曽��:�8�m������A�������ui�H����W��{�����+��_��݅ֱ�w⋆v��1���@=z���T_a����N��O�������5�������ur�O�8�8�S��փs?���!�xmYc�e[�m~��7����36�G(�<&y R�Q�(b�cĎj[���>��M�*��^�%n�:�_�~*.Vҕ퐎U��O���n����� �xD���WI��a��G�7�̃.��^>wDڏ���n������?ҟ�����{Z؄�v��j��צ����{�օ����q<=�Ph�W�?4!��=�h�qv�Kﱙ�qt��_�Hb�:Z�z��Lו�N\�'�|��s_X�Vy��_���^�k���5i�!��HL/vG?�3���N*�3R���I���H���錃����q�PS�ԥntt������Kfz�����}lȵ�B{#���y��o�g�5�ܟ��cpڜ��������O���g��r�-��N�����Ɲ����}m�u�\�Z�/ۦ2^}H��I��/}��������vzǎ�������WF�F�>p̒��r���y���������O8��?엽�%��g��{(B��~p]1G���������g(���w7n�����$�{�j�Nz:m �.��eCX���0�z��R�<l�h����R�_ �Da��$��ꦌ�nJE�����	^C�ta2iHGWW���ѐ�����dڙ/RN�}`nN���p���X�ue��Cȫ{y+�\��������k��Vr�^;8�슲��HN�ϼ���p�'꿕��P�.�D�MBΉ������?����&2�P?�jtE�XJ%�]䛑�3:���k紓nC�_�[�,�MRN6l��~Г�z�R:װ������K���FĲ�Qk�m��\�V��k�|�p=�����64x��"U��BzFJ�tȯ���ײ
	��B�f���v�ܬ��94�:�����q-��x~��q���⠮�ج�Ys�Ts�M��2>�������D��������xHY��'�?��/�� �BQ1~=uf#�stE�FV��޿i�Z-p,�n����P�t�TP!rh[>*.u��J�/�
<{�N颙�f�����@�9��_�C�< ��RD�.�Um�N�U	�g��)�����/�� T!t��.ZU!OD���)X��I�Ĺ?�2�љp=���[���V����Y7vZ���KAP�4Y���Q�|���I^��9�:���д�p(�~:��w"���V�P��A�s���~.���tT
^�[��⧟~�?3u      �   �   x�eл�@�������#g�L�a�O�̟s�Xs,�|��_������%>�������.�]<�C��S���7��-��[���W�t��t��K]� ��E�> ]��ѥ���ܫ}�����x�in���NL�r�      �   �  x�}��n�@���S̭ɥbm��HHC��AST��	^��]kwQDOy��z��y�>Iǐ�Icb!lK��ٙ��Ln?�����hT�![��Y����v>۽E��d/��ܣ�[��Y�ME�:E��f�Q�Ҙ-r����O������q���Y��]j/�2�P���}�$�hD6z���6d��
<*,C���]6������pE��1�GǆK�(m0+� �-=��H�s�L̃s���@��B�p/}�mi�s�5��tQ��a!�\��1UR.�n�;���1)���w��'�}��3`g���J�U��Q�Õ��y�a�g@�T2�FI-y��m6�/�Z\���O��Q�Ns62Ei��	�Mm>�&y����c��S�5��vk,���<f�����#T4	����@:T�Ԕ,�k�Լe�0�M�a�&s0U��*?5ka�Gq,f��]RZ�D�n`�S���_Kw���hSm�B(e)^�p#tI����XZ�a*�^>�:����Ks|���'��h�Y`+jSSiml��@�F	�Q	�
���F�gd��H�8Y9(����/`���֪ Z��pp!��n黡�h��|�U�~>=�M�6�E1#V�0{���B�UY�H6�A7|QB��Ҋjpw[Ik��%��ުI�0��V�D�,�,��bҰ�J�7�!�hٟ�y�N���}�����`      �   Z   x�%̻�0���< ���p�����}�t:e�AH�{h���*Fd���\rB�Z0#�W-p����ב\����q9妷%��;      �   ,	  x���[S�Jǟ�O���1����U�%�XL��V���XV,i8���O�=ɒ	�b��秾��Gߗ���uu�����diS�g�+/��,;�؞�7M��̾Ok��PzǛ`�r��x��+�H�;�Y���EnM�|o��ab	U�p���kʊ��kˊ�u�K �����}%2�� �	v�v	�� �Dk=�#0��EV0�A)��X%쇦�E�1�4��X�Ch	����ܤ ��?8�F�/�U����B2�	B�^��bm4��zO�~	~ǟ�ġ�m�#7t�X�������U�f�j��d��Eq /݉�&H��{��%�c��6〠U��+c+b��r��Pa��[������7:X�JW�k��x���ɬ#�L�Zfd	�&��0�L��5���X�Ґ���0T��V��U�$L�X��M��[qJ��!zHa������@-�`6�@�CH!��:e����}��T!߰��%�$�%]�8�c(vB	�B+���(P��$��B�0k6%tA�Z7,����U��*9@���,:�3�-@>|*M��+s����g	��U��q�	&�O��N�**�
@)� "���Vn�+;��Lz�ׂ���� MF�_ij5!@��UiQ��6u��3�eM��f�֠��9(_��R�:�R*d]B�;�^��ȳ����P�[��ߠ��z������xa-X���=��e�v��F���#ƹ�׳��Bd1WͷI��>����Dw��
��f��'.59%*��9�jV���ox��U����!��R��W���Q!�� �n�{�a�5��8�CX ��f�}�xnb僆1,��b�z��N��>��<�2H����=I+��u�J��PS6�~�%�Z� �n���پ���Dz���ۖ ���/�+.v�FIh�4b�f�e���.|k�T�z@�	��<<��,���R �y���AAB���^�8�茔萪�>ɘ�ϰ�M�MٌHGM�Ù�T�ײE��V���h�ۤԤƈ�&@�2�ƅq�P&A�jγ������F�ɹz�O�7t+����ǌ�+h.�b+G�^���tuق�����O]�����~���8���JzU�D�F�Wࠞ� �P!|��tf�'^���qhЃ�{��J�X;yɠ���ϊ�i��ۋR���l$וIDtڽ>%2���U.��0p�Xb,���k�i�$��(�uZr=p�6
z��Yp�������q�h8ԫ�Y����#9�)��������=RQ�2-�z�.ame�t�َCk0u�d�<�G��iG�f��l���q�4�Rd��"�%����ra�j�+�%=��X�ȴ�����Yw�8ܦ��å���	����1�3�J�1�n�VD�*�:+jQ��?Bow������f'%��Y���TS��8�;��S����Y6y�ѡ�34X�	�h���w��/����6E�;H�^'s��D9Tt�}�����X}�8����B,C[�?Rf�	zc\��C���E�5�v۷�>�՘���#�����Ƭ�S��J^A
��������Fi�9�BW���`����u��բ7W���pT�����Z���G@G�@�e�7-8{�����귀�g'�X�Iw���+8��3���P�N��7hhI�	8ܭ�������5M�qT���-8�����}x����\�m"��^���@^a�_,B:ơ'�t@-%����- H�����&���d% ���v%��,��ߥ���/��2i�Vn(ýf��?(q��n��,X�Ȏ�����3����Q�p.���|:c���ѩ2���|a�X;s�8�����ؼ9,ӝ.X)E,������e�jj��1J`<�:̼�����+�l~�_� 4y���E��O�O���@S�	�C҆�������W��@����r�D��䪈B����epg�˹~73�q�W%�D�r���l�ɫ'Pl^s����t8�JWr��ڸg��V���F�aƣ�Z�R�RVE�=��jopTm�މ|��8'ە{�|M��9ɫ���������i����Ỽ^�e<����3�d��u����@d��w�/d�p��}j]np/bg���W\�U�0_�_���=��<u�f��^[������,���x~�G��?+����'A�����^�����7����؂�#;&5�;X�YŪX�Dwh.=��6'�`��m,/��j�u��,;���u��,v�.��;>����x�5������NM��C,�{��˗��rC      �   �   x�m��r�@Ek�c[o�$i(�����G$-	;r������z����*���V��]��/�@H*l����F,�u�CS�V`�HԎ���<��oB���j3$���]��4��	PJ9�k/S��U�zo�<Q�g��
Eq�!���)��0ǆ��|��L*�@84�nL5����8֐w$����9Qs��wDp����gչ��=���dlHV6���q%�h�ʾO˲� ��_      �   �   x�m�M�@�ϳ��_�<D"-ѥ�Pb�*ҿO�c��9>�3s0�`M;i��,u�@���Ð6wW���(�mO�'�輊k� �W��'�!6��0h�&��:��jk9�XQk*���A��q�[7��u8�{7�c��=:~%+�BMRڊ}� ��⺟�<��:6V�K�+X�V0�x8��f?��W�׋�r�L�S��R�	`��m      �   �  x�}�]k�0���_���wַt1Xבue-�tc���vb��r����I^��\=<���hy��Ç�k��n��w��j��r�QN�d\S#���Ah���ջ���!�l9�8��smQ�n�t��On�Ѧ��eT���F	)8Y%ҏ�R`mkח��_��Т/�r�QIlJj��E�AjZ&ɢw�����o�q�YcQ�(1dze�T#�FnZ��������Э�n�L�1ή�ߔi��
�r~{�09�.���8�>��GhZe�.��n~�Dh7j9��a�ᯛ�$���J��Y.3�gTpl��*���������1��ܦ�QE�\���!��Z��i%����!g���.�"e/�傸���M���|YA��Ҙ+B�,�K�a��5���޸��j?�$��Z(���؎�R�s���y��[psT��,*Q��J�H�#8m�`\�	C�=`��4�̆� �`C��дJ��d캐ذ���[\8NA։���g�*���`�m%Ϩ���
�>a�:C��d��cu!�"D�q$�VZ��\�z �%d*]��ԧ4�FjФ����MB�S�x�6�����F5R�'Jq����0�UJC��93�rc$��H��+�~�-*�JfR�P�ޓǯ��x�6�+��b_.>�7tS�ң�A�m�-��T�����ۊz'�x?�u6��� ���      �   W   x�U�1� Dњ=�aVP(m������Dc�ӽ��8��<���'Oql�3�U~]0�PK�C��$z�rTxX1"G�Q��_��\��݉Z;�      �   :   x�q�300�4Tp*�I��tr�B@bF��1#��1�B�"f�ih�4����� r��      �   W  x�u�;R1��x�.P���$a�q�s ���#jm'�|��o#X�fy�1b�6����p|��oߏѶ���1� ,�c|A���i�q|O#al�c�0	&�$؟��Ѷ"�3a>!�q\.e�,�v�u�~�U�VAbK/`#l��<��!�`'�I0-�c�c|@�Y[���K�tL�hŲ���)T�G��]���S;h�m���.�xL��}}I�)��xƸ,i���)�h��f�%�1�X��YY�������S�c=��^�1������IZ2�߫���'�	���`>`>Ep�q������T�:og��8�x�x�n��������}��?O��     