from django.db import connection


class Lukisan():

    def getAll(self):
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO sipelu")
            cursor.execute("SELECT * FROM lukisan")
            res = self.serialize(cursor)

        return res

    def getOne(self, id_lukisan):
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO sipelu")
            cursor.execute(
                "SELECT * FROM lukisan WHERE id_lukisan = %s", [id_lukisan])
            res = self.serialize(cursor)
        return res[0]

    def serialize(self, cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
