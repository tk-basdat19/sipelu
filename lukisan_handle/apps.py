from django.apps import AppConfig


class LukisanHandleConfig(AppConfig):
    name = 'lukisan_handle'
