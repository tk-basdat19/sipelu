from django.shortcuts import render, redirect
from lukisan_handle.models import Lukisan
from user_handle.models import Staff
from user_handle.decorator import login_required
from user_handle.auth import is_auth
# Create your views here.


@login_required
def lukisan_detail(request, id):
    lukisan = Lukisan().getOne(id)
    return render(request, "lukisan_handle/lukisan_detail.html", {
        "lukisan": lukisan
    })


@login_required
def list_sewa(request):

    sewa = Staff().get_sewa()

    return render(request, "lukisan_handle/list_sewa.html", {
        "list_sewa": sewa
    })


@login_required
def list_transaksi(request):

    transaksi = Staff().get_transaksi()

    return render(request, "lukisan_handle/list_transaksi.html", {
        "list_transaksi": transaksi
    })
