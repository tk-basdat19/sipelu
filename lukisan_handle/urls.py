from django.urls import path, include
from .views import *

app_name = "lukisan"

urlpatterns = [
    path('lukisan/<id>', lukisan_detail, name="detail"),
    path('list_sewa', list_sewa, name="list_sewa"),
    path('list_transaksi', list_transaksi, name="list_transaksi")
]
