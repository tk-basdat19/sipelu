# Django Starter Template

# Setup Postgree

- Buat database di local
- Masukkan data database ke settings.py di variable DATABASES

# restore database

- asumsi sudah install psql
- buat database terlebih dahulu dengan nama "lukisan"
  ```
    createdb nama_user lukisan
  ```
- restore file lukisan.sql, asumsi ada di local, jika di cloud, masukkan ip address nya

  ```
  pg_restore -d lukisan -O -U nama_user -h localhost lukisan.sql
  ```

- sumber dari http://www.boxtricks.com/how-to-restore-a-postgres-database-with-a-different-owner/

# Instalasi

- clone repository ini dengan cara
  ```
    https://gitlab.com/tk-basdat19/sipelu.git
  ```
- masuk ke folder django-starter, nama folder dapat bebas diganti setelah di clone
  ```
  cd django-starter
  ```
- inisiasi git di folder yang kamu buat
  ```
  git init
  ```
- buat virtual environment
  ```
  python -m venv env
  ```
  atau
  ```
  python -m virtualenv env
  ```
- aktifkan virtual environment
  - untuk windows
    ```
    env/Scripts/activate.bat
    ```
  - untuk linux/mac
    ```
    source env/bin/activate
    ```
- install semua requirement yang ada, pastikan kamu berada di folder yang sama dengan file requirements.txt
  ```
  pip install -r requirements.txt
  ```
- migrate
  ```
  python manage.py migrate
  ```
- cek apakah sudah bisa runserver
  ```
  python manage.py runserver
  ```

# informasi AKUN

- STAFF
  - no_staff = STF0000001
  - password = test12345
- Pengguna
  - id_pengguna = USR0000001
  - password test12345
